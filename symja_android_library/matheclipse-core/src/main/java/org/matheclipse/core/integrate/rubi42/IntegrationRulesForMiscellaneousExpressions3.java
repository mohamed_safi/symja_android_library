package org.matheclipse.core.integrate.rubi42;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi42.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi42.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * IntegrationRules rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class IntegrationRulesForMiscellaneousExpressions3 { 
  public static IAST RULES = List( 
SetDelayed(CotQ($p(u)),
    SameQ(Head(u),$s("Cot"))),
SetDelayed(SecQ($p(u)),
    SameQ(Head(u),$s("Sec"))),
SetDelayed(CscQ($p(u)),
    SameQ(Head(u),$s("Csc"))),
SetDelayed(SinhQ($p(u)),
    SameQ(Head(u),$s("Sinh"))),
SetDelayed(CoshQ($p(u)),
    SameQ(Head(u),$s("Cosh"))),
SetDelayed(TanhQ($p(u)),
    SameQ(Head(u),$s("Tanh"))),
SetDelayed(CothQ($p(u)),
    SameQ(Head(u),$s("Coth"))),
SetDelayed(SechQ($p(u)),
    SameQ(Head(u),$s("Sech"))),
SetDelayed(CschQ($p(u)),
    SameQ(Head(u),$s("Csch"))),
SetDelayed(TrigQ($p(u)),
    MemberQ(List($s("Sin"),$s("Cos"),$s("Tan"),$s("Cot"),$s("Sec"),$s("Csc")),If(AtomQ(u),u,Head(u)))),
SetDelayed(InverseTrigQ($p(u)),
    MemberQ(List($s("ArcSin"),$s("ArcCos"),$s("ArcTan"),$s("ArcCot"),$s("ArcSec"),$s("ArcCsc")),If(AtomQ(u),u,Head(u)))),
SetDelayed(HyperbolicQ($p(u)),
    MemberQ(List($s("Sinh"),$s("Cosh"),$s("Tanh"),$s("Coth"),$s("Sech"),$s("Csch")),If(AtomQ(u),u,Head(u)))),
SetDelayed(InverseHyperbolicQ($p(u)),
    MemberQ(List($s("ArcSinh"),$s("ArcCosh"),$s("ArcTanh"),$s("ArcCoth"),$s("ArcSech"),$s("ArcCsch")),If(AtomQ(u),u,Head(u)))),
SetDelayed(SinCosQ($p(f)),
    MemberQ(List($s("Sin"),$s("Cos"),$s("Sec"),$s("Csc")),f)),
SetDelayed(SinhCoshQ($p(f)),
    MemberQ(List($s("Sinh"),$s("Cosh"),$s("Sech"),$s("Csch")),f)),
SetDelayed(CalculusQ($p(u)),
    MemberQ(List($s("D"),$s("Integrate"),$s("Sum"),$s("Product"),$s("Int"),$s("Dif"),$s("Integrate::Subst")),Head(u))),
SetDelayed(CalculusFreeQ($p(u),$p(x)),
    If(AtomQ(u),True,If(Or(And(CalculusQ(u),SameQ(Part(u,C2),x)),HeldFormQ(u)),False,Catch(CompoundExpression(Scan(Function(If(CalculusFreeQ(Slot1,x),Null,Throw(False))),u),True))))),
SetDelayed(HeldFormQ($p(u)),
    If(AtomQ(Head(u)),MemberQ(List($s("Hold"),$s("HoldForm"),$s("Defer"),$s("Pattern")),Head(u)),HeldFormQ(Head(u)))),
SetDelayed(InverseFunctionQ($p(u)),
    Or(Or(Or(Or(LogQ(u),And(InverseTrigQ(u),Equal(Length(u),C1))),InverseHyperbolicQ(u)),SameQ(Head(u),$s("Mods"))),SameQ(Head(u),$s("PolyLog")))),
SetDelayed(TrigHyperbolicFreeQ($p(u),$p(x,SymbolHead)),
    If(AtomQ(u),True,If(Or(Or(TrigQ(u),HyperbolicQ(u)),CalculusQ(u)),FreeQ(u,x),Catch(CompoundExpression(Scan(Function(If(TrigHyperbolicFreeQ(Slot1,x),Null,Throw(False))),u),True))))),
SetDelayed(InverseFunctionFreeQ($p(u),$p(x,SymbolHead)),
    If(AtomQ(u),True,If(Or(Or(Or(InverseFunctionQ(u),CalculusQ(u)),SameQ(Head(u),$s("Hypergeometric2F1"))),SameQ(Head(u),$s("AppellF1"))),FreeQ(u,x),Catch(CompoundExpression(Scan(Function(If(InverseFunctionFreeQ(Slot1,x),Null,Throw(False))),u),True))))),
SetDelayed(NegativeCoefficientQ($p(u)),
    If(SumQ(u),NegativeCoefficientQ(First(u)),MatchQ(u,Condition(Times($p(m),$p(v,true)),And(RationalQ(m),Less(m,C0)))))),
SetDelayed(RealQ($p(u)),
    Condition(MapAnd($s("Integrate::RealQ"),u),ListQ(u))),
SetDelayed(RealQ($p(u)),
    Condition(PossibleZeroQ(Im(N(u))),NumericQ(u))),
SetDelayed(RealQ(Power($p(u),$p(v))),
    And(And(RealQ(u),RealQ(v)),Or(IntegerQ(v),PositiveOrZeroQ(u)))),
SetDelayed(RealQ(Times($p(u),$p(v))),
    And(RealQ(u),RealQ(v))),
SetDelayed(RealQ(Plus($p(u),$p(v))),
    And(RealQ(u),RealQ(v))),
SetDelayed(RealQ($($p(f),$p(u))),
    If(MemberQ(List($s("Sin"),$s("Cos"),$s("Tan"),$s("Cot"),$s("Sec"),$s("Csc"),$s("ArcTan"),$s("ArcCot"),$s("Erf")),f),RealQ(u),If(MemberQ(List($s("ArcSin"),$s("ArcCos")),f),LE(CN1,u,C1),If(SameQ(f,$s("Log")),PositiveOrZeroQ(u),False)))),
SetDelayed(RealQ($p(u)),
    False),
SetDelayed(PosQ($p(u)),
    PosAux(TogetherSimplify(u))),
SetDelayed(PosAux($p(u)),
    If(RationalQ(u),Greater(u,C0),If(NumberQ(u),If(PossibleZeroQ(Re(u)),Greater(Im(u),C0),Greater(Re(u),C0)),If(NumericQ(u),Module(List(Set(v,N(u))),If(PossibleZeroQ(Re(v)),Greater(Im(v),C0),Greater(Re(v),C0))),If(ProductQ(u),If(PosAux(First(u)),PosAux(Rest(u)),Not(PosAux(Rest(u)))),If(SumQ(u),PosAux(First(u)),True)))))),
SetDelayed(NegQ($p(u)),
    If(PossibleZeroQ(u),False,Not(PosQ(u)))),
SetDelayed(LeadTerm($p(u)),
    If(SumQ(u),First(u),u)),
SetDelayed(RemainingTerms($p(u)),
    If(SumQ(u),Rest(u),C0)),
SetDelayed(LeadFactor($p(u)),
    If(ProductQ(u),LeadFactor(First(u)),If(ImaginaryQ(u),If(SameQ(Im(u),C1),u,LeadFactor(Im(u))),u))),
SetDelayed(RemainingFactors($p(u)),
    If(ProductQ(u),Times(RemainingFactors(First(u)),Rest(u)),If(ImaginaryQ(u),If(SameQ(Im(u),C1),C1,Times(CI,RemainingFactors(Im(u)))),C1))),
SetDelayed(LeadBase($p(u)),
    Module(List(Set(v,LeadFactor(u))),If(PowerQ(v),Part(v,C1),v))),
SetDelayed(LeadDegree($p(u)),
    Module(List(Set(v,LeadFactor(u))),If(PowerQ(v),Part(v,C2),C1))),
SetDelayed(LT($p(u),$p(v)),
    And(And(RealNumericQ(u),RealNumericQ(v)),Less(Re(N(u)),Re(N(v))))),
SetDelayed(LT($p(u),$p(v),$p(w)),
    And(LT(u,v),LT(v,w))),
SetDelayed(LE($p(u),$p(v)),
    And(And(RealNumericQ(u),RealNumericQ(v)),LessEqual(Re(N(u)),Re(N(v))))),
SetDelayed(LE($p(u),$p(v),$p(w)),
    And(LE(u,v),LE(v,w))),
SetDelayed(GT($p(u),$p(v)),
    And(And(RealNumericQ(u),RealNumericQ(v)),Greater(Re(N(u)),Re(N(v))))),
SetDelayed(GT($p(u),$p(v),$p(w)),
    And(GT(u,v),GT(v,w))),
SetDelayed(GE($p(u),$p(v)),
    And(And(RealNumericQ(u),RealNumericQ(v)),GreaterEqual(Re(N(u)),Re(N(v))))),
SetDelayed(GE($p(u),$p(v),$p(w)),
    And(GE(u,v),GE(v,w))),
SetDelayed(IndependentQ($p(u),$p(x)),
    FreeQ(u,x)),
SetDelayed(FreeFactors($p(u),$p(x)),
    If(ProductQ(u),Map(Function(If(FreeQ(Slot1,x),Slot1,C1)),u),If(FreeQ(u,x),u,C1))),
SetDelayed(NonfreeFactors($p(u),$p(x)),
    If(ProductQ(u),Map(Function(If(FreeQ(Slot1,x),C1,Slot1)),u),If(FreeQ(u,x),C1,u))),
SetDelayed(SplitFreeFactors($p(u),$p(x)),
    If(ProductQ(u),Map(Function(If(FreeQ(Slot1,x),List(Slot1,C1),List(C1,Slot1))),u),If(FreeQ(u,x),List(u,C1),List(C1,u))))
  );
}
